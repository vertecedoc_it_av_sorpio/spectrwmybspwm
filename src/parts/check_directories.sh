#! /bin/bash 

declare -a dots
dots=($(ls -a ../home/.config))
unset dots[0]
unset dots[1]

declare -a exist
exist=($(ls -a $HOME/.config/ ))
unset exist[0]
unset exist[1]

for i in "${dots[@]}"
do	
	if ["${exist[@]}" == *"$i"* ];
	then
		mkdir $HOME/.config/"$i"/"previous configuration"
		mv $HOME/.config/"$i"/* $HOME/.config/"$i"/"previous configuration"
	else
		return "$i"
	fi
done

