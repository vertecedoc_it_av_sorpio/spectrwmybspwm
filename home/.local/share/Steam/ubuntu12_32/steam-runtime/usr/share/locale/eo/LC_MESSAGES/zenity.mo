��    {      �  �   �      h
  &   i
     �
     �
  "   �
     �
     �
     �
       #   *  "   N     q  !   �     �  
   �     �     �     �     �  #        0     4     L     k     �     �     �     �     �     �                <     S     g     m     {  
   �     �     �     �     �     �     �  
   �                )     6     B     O     U     k     r  	   �     �  
   �     �  	   �     �     �     �     �  
   �  	             +  !   F     h          �     �     �     �     �     �               )     =     R     c     r     �     �     �     �     �     �          "     <     Q     c     u     �     �     �     �     �                %     -  	   3  I   =  �   �  �   y  %   q     �     �  	   �     �     �     �     �     �  @   �  �   "     �     
  #        ?  V  R  (   �  
   �     �  &   �          5  "   L     o  "   �     �     �  '   �                    /     @     Q  #   d     �     �  !   �     �     �     �          !     5     D     \     s     �     �     �     �  
   �     �     �     �          "     '     ?     W     e     {     �     �     �     �     �     �     �     �     �          	  	        (     4     =     E  
   T  
   _     j     �  '   �     �     �     �          +     ?     W     q     �     �     �     �     �     �          #     :     L     l     �     �     �     �     �     
      !      6   "   O      r      �      �      �      �      �   	   �      �   
    !  I   !  �   U!    F"  &   M#     t#     �#     �#     �#     �#     �#     �#     �#  E   �#  �   $     �$     %  #   %  ]   B%     H   K      '       l          5              ]       P      j       {       *              i   C          .   ^   B   s   o   !   G          y   e       L       S   +   ;              r   0   ,   F       z   d                               [                       )      I       T            (           U   D   h   $   =   2   1           @   
   -   _          Q   \           u   V   "   t          f       `       k   8   :   v   a      q   4   %           p   W   N           9   	       Y   x      O   c   7   w          n   g   R   M   /   <   E   3           #       &   b      A   J      Z       6   m       ?   >       X       --%s is not supported for this dialog
 About zenity Activate save mode Add a new Calendar in forms dialog Add a new entry Adjust the scale value All updates are complete. Allow changes to text Allow multiple files to be selected Allow multiple rows to be selected An error has occurred. Are you sure you want to proceed? COLUMN C_alendar: Calendar field name Calendar options Calendar selection Color selection options Could not parse message from stdin
 DAY Display calendar dialog Display color selection dialog Display error dialog Display file selection dialog Display forms dialog Display info dialog Display list dialog Display notification Display password dialog Display question dialog Display the username option Display warning dialog Enable html support Error Error options FILENAME Field name File selection options Forms dialog options General options HEIGHT Hide Cancel button Hide a specific column Hide value Hides the column headers ICONPATH Info options Information List options MONTH Miscellaneous options NUMBER Notification icon options Open file PATTERN PERCENTAGE Password dialog options Password: Print version Progress Question Question options Running... SEPARATOR Select a date from below. Select items from the list Select items from the list below. Set initial percentage Set initial value Set maximum value Set minimum value Set step size Set the calendar day Set the calendar month Set the calendar year Set the color Set the column header Set the dialog text Set the dialog title Set the filename Set the height Set the notification text Set the text font Set the width Set the window icon Show calendar options Show color selection options Show error options Show file selection options Show forms dialog options Show general options Show info options Show list options Show miscellaneous options Show notification icon options Show password dialog options Show question options Show the columns header Show the palette Show warning options TEXT TIMEOUT TITLE Text View This option is not available. Please see --help for all possible usages.
 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 This program is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 Two or more dialog options specified
 Type your password URL Username: VALUE WIDTH Warning Warning options YEAR You must specify a dialog type. See 'zenity --help' for details
 You should have received a copy of the GNU Lesser General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. Zenity notification _Enter new text: could not parse command from stdin
 translator-credits Project-Id-Version: zenity
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=zenity&keywords=I18N+L10N&component=general
POT-Creation-Date: 2011-12-21 12:23+0000
PO-Revision-Date: 2011-12-21 20:05+0100
Last-Translator: Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>
Language-Team: Esperanto <gnome-l10n-eo@lists.launchpad.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bits
Language: eo
Plural-Forms: nplurals=2; plural=(n != 1)
X-Generator: Virtaal 0.7.1-rc1
X-Launchpad-Export-Date: 2010-12-11 17:28+0000
X-Project-Style: gnome
 --%s estas ne subtenita por tiu dialogo
 Pri Zenity Enŝalti konserv-reĝimon Aldoni novan kalendaron en formdialogo Aldoni novan elementon Agordi la skal-valoron Ĉiij ĝisdatigoj estas kompletaj. Permesi ŝanĝon de teksto Permesi elekton de pluraj dosieroj Permesi elekton de pluraj vicoj Eraro okazis. Ĉu vi estas certa ke vi volas procedi? KOLUMNO K_alendaro: Kalendara nomo de kampo Kalendar-agordoj Kalendaro-elekto Kolorelekt-agordoj ne eblis analizi mesaĝon de stdin
 TAGO Montri kalendardialogon Montriu la kolor-elektad-dialogon Montri erardialogon Montri dosierelekt-dialogon Montri formdialogon Montri informdialogon Montri listdialogon Montri sciigon Montri pasvort-dialogon Montri demand-dialogon Montri la unzantonom-agordon Montri avert-dialogon Ebligi HTML-subtenon Eraro Erar-agordoj DOSIERNOMO Nomo de kampo Dosierelekt-agordoj Formdialogaj opcioj Ĝeneralaj agordoj ALTO Kaŝi "Rezigni"-butonon Kaŝi specifan kolumnon Kaŝi valoron Kaŝi kolumnotitolojn PIKTOGRAMVOJO Informo-agordoj Informo List-agordoj MONATO Diversaj opcioj NOMBRO Sciig-bildsimbolaj opcioj Malfermi dosieron ŜABLONO ELCENTO Pasvortdialog-opcioj Pasvorto: Pres-versio Progreso Demando Demand-agordoj Rulanta... APARTIGILO Elekti daton malsupre. Elekti elementojn de la listo Elekti elementojn de la listo malsupre. Agordi komencan elcenton Agordi komencan valoron Agordi maksimuman valoron Agordi minimuman valoron Agordi paŝ-grandon Agordi la kalendartagon Agordi la kalendarmonaton Agordi la kalendarjaron Agordi la koloron Agordi la kolumnotitolon Agordi la dialogtekston Agordi la dialog-titolon Agordi la dosiernomon Agordi la alton Agordi la sciigotekston Agordi la teksttiparon Agordi la larĝon Agordi la fenestran piktogramon Montri kalendar-agordojn Montri kolorelekt-agordojn Montri erar-agordojn Montri dosierelekt-agordojn Montri formdialogajn opciojn Montri ĝeneralajn agordojn Montri inform-agordojn Montri list-agordojn Montri diversajn opciojn Montri sciig-bildsimbolajn opciojn Montri pasvortdialog-opciojn Montri demand-agordojn Montri la kolumnotitolon Montri la paletron Montri avert-agordojn TEKSTO TEMPOLIMO TITOLO Tekst-vido Tiu agordo ne estas disponebla. Bonvole rigardu --help por eblaj uzadoj.
 Tiu programo estas distribuite kun la espero ke ĝi estos utila, sed SEN IA AJN GARANTIO; eĉ sen la implica garantio de NEGOCEBLO aŭ ADAPTADO AL IU APARTA CELO. Vidu la Malsuperan Ĝeneralan Publikan Permesilon de GNU por pli da detaloj.
 Tiu programo estas libera programaro; vi povas distribui kaj/aŭ modifi ĝin laŭ la kondiĉoj de la Malsupera Ĝenerala Publika Permesilo de GNU eldonite de 'Free Software Foundation'; aŭ en la 2a versio de la permesilo aŭ (laŭ via volo) en iu sekva versio.
 Specifis du aŭ pli dialogajn opciojn
 Enigu vian pasvorton URL Uzantonomo: VALORO LARĜO Averto Avert-agordoj JARO Vi devas specifi specon de dialogo. Vidu 'zenity --help' por detaloj
 Vi devintus ricevi kopion de la Malsupera Ĝenerala Publika Permesilo de GNU kun tiu ĉi programo; se ne, skribu al la Fondaĵo Libera Programaro: Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, Usono. Zenity-sciigo _Enigi novan tekston: ne eblis analizi komandon de stdin
 Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>, 2010, 2011.
Tiffany ANTOPOLSKI < >, 2011. 