#!/bin/bash

DIR=`dirname $0`
cd ${DIR}

# Steam runtime 'heavy' is a newer runtime than the default Steam runtime (scout)
# steamwebhelper and libcef.so are built in this newer environment,
# and the needed libraries from heavy are provided here.
# steamwebhelper is compiled to run in the composite runtime made up of heavy+scout(+host)
# See http://repo.steampowered.com/steamrt/steamrt-heavy/ for details

# STEAM_RUNTIME_HEAVY is a debug tool that can be used in the same way as STEAM_RUNTIME in steam.sh,
# to override the location of the heavy runtime libraries

# Skip setting STEAM_RUNTIME_HEAVY if STEAM_RUNTIME=0
if [ -z "$STEAM_RUNTIME_HEAVY" -a "$STEAM_RUNTIME" != "0" ]; then
    export STEAM_RUNTIME_HEAVY=./steam-runtime-heavy
fi
echo "STEAM_RUNTIME_HEAVY: ${STEAM_RUNTIME_HEAVY:-Disabled}"

export LD_LIBRARY_PATH=".:${STEAM_RUNTIME_HEAVY}:${LD_LIBRARY_PATH}:${DIR}"

./steamwebhelper "$@"
